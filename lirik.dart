import 'dart:async';

void main() {
  print("Title:Lily (Alan Walker)");
  print("");

  var timer =
      Timer(Duration(seconds: 1), () => print("Lily was a little girl"));
  var timer1 =
      Timer(Duration(seconds: 2), () => print("Afraid of the big, wide world"));
  var timer2 = Timer(
      Duration(seconds: 3), () => print("She grew up within her castle walls"));
  var timer3 =
      Timer(Duration(seconds: 4), () => print("Now and then she tried to run"));
  var timer4 = Timer(Duration(seconds: 5),
      () => print("And then on the night with the setting sun"));
  var timer5 =
      Timer(Duration(seconds: 6), () => print("She went in the woods away"));
  var timer6 = Timer(Duration(seconds: 7), () => print("So afraid, all alone"));
  var timer7 = Timer(
      Duration(seconds: 8), () => print("They warned her, don't go there"));
  var timer8 = Timer(Duration(seconds: 9),
      () => print("There's creatures who are hiding in the dark"));
  var timer9 =
      Timer(Duration(seconds: 10), () => print("Then something came creeping"));
  var timer10 = Timer(
      Duration(seconds: 11), () => print("It told her, don't you worry just"));
  var timer11 = Timer(Duration(seconds: 12), () => print("~~~~REFF~~~!"));
  var timer12 =
      Timer(Duration(seconds: 13), () => print("Follow everywhere I go"));
  var timer13 = Timer(Duration(seconds: 14),
      () => print("Top of all the mountains or valley low"));
  var timer14 = Timer(Duration(seconds: 15),
      () => print("Give you everything you've been dreaming of"));
  var timer15 =
      Timer(Duration(seconds: 16), () => print("Just let me in, ooh"));
  var timer16 = Timer(
      Duration(seconds: 17),
      () => print(
          "Everything you're wanting gonna be the magic story you've been told"));
  var timer17 = Timer(Duration(seconds: 18),
      () => print("And you'll be safe under my control"));
  var timer18 =
      Timer(Duration(seconds: 19), () => print("Just let me in, ooh"));
}
